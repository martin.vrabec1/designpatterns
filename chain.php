<?php
    use App\ChainOfResponsibility\DogHandler;
    use App\ChainOfResponsibility\MonkeyHandler;

    include_once 'vendor/autoload.php';

    $items = ["bread", "leaf", "meat", "banana"];

    $dogHandler = new DogHandler();
    $monkeyHandler = new MonkeyHandler();

    $monkeyHandler->setNext($dogHandler);

    foreach ($items as $item) {
        $response = $monkeyHandler->handle($item);
        echo $response;
    }
?>