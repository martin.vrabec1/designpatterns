<?php

use App\Iterator\FibonacciApp;
use App\Iterator\Iterator;
use PHPUnit\Framework\TestCase;

final class FibonacciTest extends TestCase {

    public function testFibonacci():void {
        $fibonacci = FibonacciApp::generate(4);

        $this->assertSame($fibonacci,[1,1,2,3,5]);
    }

}