<?php 
use PHPUnit\Framework\TestCase;
use App\Singleton;

    class SingletonTest extends TestCase {
        public function testGetInstanceReturnSameInstance()
        {
            $instance1 = Singleton::getInstance();
            $instance2 = Singleton::getInstance();

            $this->assertInstanceOf(Singleton::class, $instance1);
            $this->assertInstanceOf(Singleton::class, $instance2);
            $this->assertSame($instance1, $instance2);
        }

        public function testCannotInstantiateDirectly()
        {
            $reflection = new ReflectionClass(Singleton::class);
            $constructor = $reflection->getConstructor();

            $this->assertFalse($constructor->isPublic());
        }
    }

?>