<?php

use App\State\HappyMood;
use App\State\HappyMoodFactory;
use App\State\NeutralMood;
use App\State\Person;
use PHPUnit\Framework\TestCase;

final class StateTest extends TestCase {

    private $happyMood;

    public function __construct(){
        $this->happyMood = HappyMoodFactory::getMood();
    }

    public function testHappyToNeutral() {
        $agatha = new Person('agatha', $this->happyMood);
        $agatha->insult();

        $this->assertEquals($agatha->getMood(), new NeutralMood());
    }

    public function testNeutralToHappy() {
        $agatha = new Person('agatha', new NeutralMood());
        $agatha->hug();

        $this->assertEquals($agatha->getMood(), new HappyMood());
    }

    public function testHappyToNeutralToHappy() {
        $agatha = new Person('agatha', new HappyMood());
        $agatha->insult();
        $agatha->hug();

        $this->assertEquals($agatha->getMood(), new HappyMood());
    }

}