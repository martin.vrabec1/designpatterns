<?php

include_once 'vendor/autoload.php';

use App\Bridge\BlueColor;
use App\Bridge\Circle;
use App\Bridge\RedColor;
use App\Iterator\FibonacciApp;

$redCircle = new Circle(new RedColor());
$blueCircle = new Circle(new BlueColor());

echo $redCircle->draw();
echo $blueCircle->draw();

$fiboRes = FibonacciApp::generate(4);
var_dump($fiboRes);
?>