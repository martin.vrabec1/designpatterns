<?php

namespace App\Iterator;

class FibonacciApp{

    static function generate(int $digits):array {
        $fibonacci = new Iterator;

        $toRet = [];

        foreach ($fibonacci as $index => $value) {
            $toRet[] = $value;

            if ($index >= $digits) {
                break;
            }
        }

        return $toRet;
    }

}