<?php

namespace App\Iterator;

class Iterator implements \Iterator
{
    private $previous = 0;
    private $current = 1;
    private $key = 0;

    public function current():mixed
    {
        return $this->current;
    }

    public function key():mixed
    {
        return $this->key;
    }
    public function next():void
    {
        $temp = $this->current;
        $this->current = $this->previous + $this->current;
        $this->previous = $temp;
        $this->key++;
    }

    public function rewind():void
    {
        $this->previous = 0;
        $this->current = 1;
        $this->key = 0;
    }

    public function valid():bool
    {
        return true;
    }
}
