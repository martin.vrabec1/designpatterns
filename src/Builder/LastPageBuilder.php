<?php

namespace App\Builder;

class LastPageBuilder extends AbstractPageBuilder {

    public function buildBody()
    {
        $this->page->setBody('Last page body');
    }

    public function buildHeader()
    {
        $this->page->setHeader('Last page header');
    }

    public function buildFooter()
    {
        $this->page->setFooter('Last page footer');
    }

}