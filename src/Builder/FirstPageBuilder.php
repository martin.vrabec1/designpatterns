<?php

namespace App\Builder;

class FirstPageBuilder extends AbstractPageBuilder {

    public function buildBody()
    {
        $this->page->setBody('First page body');
    }

    public function buildHeader()
    {
        $this->page->setHeader('First page header');
    }

    public function buildFooter()
    {
        $this->page->setFooter('First page footer');
    }

}