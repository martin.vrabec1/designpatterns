<?php

namespace App\Builder;

abstract class AbstractPageBuilder implements PageBuilder {

    protected Page $page;

    public function getPage(){
        return $this->page;
    }

    public function createNewPage(){
        $this->page = new Page();
    }
}