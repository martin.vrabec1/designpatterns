<?php

namespace App\Builder;

interface PageBuilder {
    public function buildHeader();
    public function buildBody();
    public function buildFooter();
}