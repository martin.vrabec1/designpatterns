<?php

namespace App\Builder;

class Compositor {
    protected AbstractPageBuilder $pageBuilder;

    public function setPageBuilder(AbstractPageBuilder $pageBuilder)
    {
        $this->pageBuilder = $pageBuilder;
    }

    public function getPage()
    {
        return $this->pageBuilder->getPage();
    }

    public function compositePage()
    {
        $this->pageBuilder->createNewPage();
        $this->pageBuilder->buildHeader();
        $this->pageBuilder->buildBody();
        $this->pageBuilder->buildFooter();
    }
}