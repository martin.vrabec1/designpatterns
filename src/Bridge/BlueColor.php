<?php

namespace App\Bridge;

class BlueColor implements Color {

    public function fill(){
        return 'filled blue';
    }

}