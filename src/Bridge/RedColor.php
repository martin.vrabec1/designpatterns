<?php

namespace App\Bridge;

class RedColor implements Color {

    public function fill(){
        return 'filled red';
    }

}