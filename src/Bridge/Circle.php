<?php

namespace App\Bridge;

class Circle implements Shape {

    private $color;

    public function __construct(Color $color){
        $this->color = $color;
    }

    public function draw(){
        return sprintf('This is %s circle', $this->color->fill());
    }

}