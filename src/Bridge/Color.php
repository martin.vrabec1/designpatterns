<?php

namespace App\Bridge;

interface Color {

    public function fill();

}