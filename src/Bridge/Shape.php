<?php

namespace App\Bridge;

interface Shape {

    public function draw();

}