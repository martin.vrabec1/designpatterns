<?php 

namespace App\Command;
use App\Command\OnState;
use App\Command\State;

class OffState implements State {

    public function turnOn()
    {
        print("\n\rBulb is turned on");
        return new OnState();
    }

    public function turnOff()
    {
        print("\n\rBulb is already off");
        return $this;
    }

}


?>