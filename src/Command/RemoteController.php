<?php 

namespace App\Command;

class RemoteController {

    private Command $command;

    public function setCommand(Command $command)
    {
        return $this->command = $command;
    }

    public function execute()
    {
        if (!$this->command) {
            throw new \Exception("Command not set");
        }
        $this->command->execute();
    }

}

?>