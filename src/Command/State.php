<?php 

namespace App\Command;

interface State{

    public function turnOn();
    public function turnOff();

}

?>