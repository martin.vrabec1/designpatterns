<?php 

namespace App\Command;
use App\Command\State;

class OnState implements State {

    public function turnOn()
    {
        print("\n\rBulb is already on");
        return $this;
    }

    public function turnOff()
    {
        print("\n\rBulb is turned off");
        return new OffState();
    }

}


?>