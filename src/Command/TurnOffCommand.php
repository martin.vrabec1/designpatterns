<?php 

namespace App\Command;

class TurnOffCommand implements Command {

    private Bulb $bulb;

    public function setBulb(Bulb $bulb)
    {
        $this->bulb = $bulb;
    }

    public function execute()
    {
        return $this->bulb->turnOff();    
    }

}

?>