<?php


namespace App\Command;
use App\Command\State;

class Bulb {
    protected State $state;

    public function __construct()
    {
        $this->state = new OffState();
    }

    public function turnOn()
    {
        $this->state->turnOn();
    }

    public function turnOff()
    {
        $this->state->turnOff();
    }
}

?>