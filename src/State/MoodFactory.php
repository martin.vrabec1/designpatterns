<?php

namespace App\State;

interface MoodFactory {
    
    public static function getMood();
}