<?php

namespace App\State;

class Person {

    private Mood $mood;

    private string $name;

    public function __construct($name, Mood $mood) {
        $this->name = $name;
        $this->mood = $mood;
    }

    /**
     * Get the value of mood
     */
    public function getMood(): Mood
    {
        return $this->mood;
    }

    /**
     * Set the value of mood
     */
    public function setMood(Mood $mood): self
    {
        $this->mood = $mood;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function insult(){
        $this->mood->insult($this);
    }

    public function hug(){
        $this->mood->hug($this);
    }

    public function say($msg){
        echo PHP_EOL.$msg;
    }
}