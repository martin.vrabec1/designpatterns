<?php

namespace App\State;

class HappyMood extends Mood {
    
    public function insult(Person $context){
        $context->setMood(new NeutralMood);
    }

    public function hug(Person $context){
        $context->say('you are nice');
    }
}