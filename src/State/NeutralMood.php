<?php

namespace App\State;
use App\State\HappyMood;

class NeutralMood extends Mood {
    
    public function insult(Person $context){
        $context->say('aaau');
    }

    public function hug(Person $context) {
        $context->setMood(new HappyMood);
    }


}