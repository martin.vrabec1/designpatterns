<?php

namespace App\State;

abstract class Mood {
    
    public function hug(Person $context){}

    public function insult(Person $context){}
}