<?php

namespace App\State;

class HappyMoodFactory implements MoodFactory {
    
    public static function getMood(){
        return new HappyMood();
    }
}