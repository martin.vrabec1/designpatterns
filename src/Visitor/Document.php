<?php

namespace App\Visitor;

class Document {

    private $pageCount;

    public function __construct(int $pageCount){
        $this->pageCount = $pageCount;
    } 

    public function accept(VisitorInterface $visitor) {
        return $visitor->visitDocument($this);
    }

    public function getPageCount() {
        return $this->pageCount;
    }
    
}