<?php

namespace App\Visitor;

class Chapter {

    private $chapter;

    private $pageCount;

    public function __construct($chapter, $pageCount){
        $this->chapter = $chapter;
        $this->pageCount = $pageCount;
    }

    public function getPageCount(){
        return $this->pageCount;
    }
    
}