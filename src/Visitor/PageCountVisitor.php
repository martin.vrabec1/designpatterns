<?php 

namespace App\Visitor;

class PageCountVisitor implements VisitorInterface {

    public function visitBook(Book $book) {
        $count = 0;

        foreach ($book->getChapters() as $chapter) {
            $count += $chapter->getPageCount();
        }

        var_dump($count);
    }

    public function visitDocument(Document $document) {
        var_dump($document->getPageCount());
    }

}