<?php

namespace App\Visitor;

class Book {

    public function accept(VisitorInterface $visitor) {
        return $visitor->visitBook($this);
    }

    public function getChapters(): array {
        return [
            new Chapter(5, 134),
            new Chapter(7, 215),
            new Chapter(2, 132)
        ];
    }
    
}