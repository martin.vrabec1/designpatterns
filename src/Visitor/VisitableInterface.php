<?php

namespace App\Visitor;

interface VisitableInterface {

    public function accept(VisitorInterface $visitor);
    
}