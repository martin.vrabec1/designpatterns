<?php

namespace App\Visitor;

interface VisitorInterface {
 
    public function visitBook(Book $book);

    public function visitDocument(Document $document);

}