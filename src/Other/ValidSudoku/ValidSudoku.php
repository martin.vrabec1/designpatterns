<?php 

namespace App\Other\ValidSudoku;

class ValidSudoku {

    public function isValidSudoku($board) {
        $set = [];

        for ($i = 0; $i < count($board); $i++) {
            for ($j = 0; $j < count($board[$i]); $j++) {
                $item = $board[$i][$j];
                if ($item === '.') continue;

                $set[] = 'l'.$i.$item;
                $set[] = 'c'.$item.$j;
                $set[] = 'l'.intdiv($i, 3).'c'.intdiv($j, 3).$item;
            }
        }
        
        return count($set) === count(array_unique($set));
    }
}

?>