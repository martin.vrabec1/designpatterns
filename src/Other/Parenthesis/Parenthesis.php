<?php

namespace App\Other\Parenthesis;

class Parenthesis {


    public function generateParenthesis($n)
    {
        $result = [];
        $this->generateValidParenthesis($n, $n, '', $result);
        return $result;
    }

    private function generateValidParenthesis($openCount, $closeCount, $current, &$result){
        if ($openCount === 0 && $closeCount === 0) {
            $result[] = $current;
            return;
        }

        if ($openCount > 0) {
            $this->generateValidParenthesis($openCount - 1, $closeCount, $current . '(', $result);
        }

        if ($closeCount > $openCount) {
            $this->generateValidParenthesis($openCount, $closeCount - 1, $current . ')', $result);
        }
    }
}