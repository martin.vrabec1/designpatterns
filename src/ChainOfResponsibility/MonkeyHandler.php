<?php 

namespace App\ChainOfResponsibility;

class MonkeyHandler extends AbstractHandler{

    public function handle($request)
    {
        if ($request === "banana"){
            return "Monkey found banana";
        }

        return parent::handle($request);
    }
}

?>