<?php 

namespace App\ChainOfResponsibility;

interface Handler {

    public function setNext(Handler $handler);

    public function handle($request);
}

?>