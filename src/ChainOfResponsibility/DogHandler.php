<?php 

namespace App\ChainOfResponsibility;

class DogHandler extends AbstractHandler{

    public function handle($request)
    {
        if ($request === "meat"){
            return "Dog found meat";
        }

        return parent::handle($request);
    }
}
