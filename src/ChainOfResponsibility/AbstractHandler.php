<?php 

namespace App\ChainOfResponsibility;

abstract class AbstractHandler implements Handler{

    private ?Handler $next = null;

    public function setNext(Handler $handler):Handler{
        $this->next = $handler;
        return $handler;
    }

    public function handle($request){
        if ($this->next){
            return $this->next->handle($request);
        }

        return null;
    }

}

?>