<?php 

namespace App\Singleton;

class Singleton {
    private static $instance = null;

    protected function __construct() {};

    public static function getInstance() : self {
        if (self::$instance === null) {
            self::$instance new self();
        }
        return self::$instance;
    }
}

?>