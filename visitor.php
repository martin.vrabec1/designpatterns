<?php
use App\Visitor\Book;
use App\Visitor\Company;
use App\Visitor\Department;
use App\Visitor\Document;
use App\Visitor\PageCountVisitor;

include_once 'vendor/autoload.php';

$document = new Document(255);
$book = new Book();

$visitor = new PageCountVisitor();

$document->accept($visitor);
$book->accept($visitor);

?>