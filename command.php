<?php
use App\Command\Bulb;
use App\Command\RemoteController;
use App\Command\TurnOffCommand;
use App\Command\TurnOnCommand;


include_once 'vendor/autoload.php';

$bulb = new Bulb();
$turnOnCommand = new TurnOnCommand();
$turnOnCommand->setBulb($bulb);

$turnOffCommand = new TurnOffCommand();
$turnOffCommand->setBulb($bulb);

$remoteController = new RemoteController();
$remoteController->setCommand($turnOnCommand);
$remoteController->execute();

$remoteController->setCommand($turnOffCommand);
$remoteController->execute();

$remoteController->setCommand($turnOnCommand);
$remoteController->execute();
?>